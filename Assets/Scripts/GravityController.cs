﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityController : MonoBehaviour {
    //重力加速度
    //constはJavaで言うfinal(定数)
    const float Gravity = 9.81f;
    //重力の適応具合
    public float gravityScale = 1.0f;
    //今回は使わない
    void Start () {}
    void Update () {
        //Vector2は二次元x,y
        //Vector3は三次元x,y,z
        Vector3 vector = new Vector3();
        //キーの入力を検知しベクトルを設定ｘ横、ｚ奥行き、ｙ高さ
        //Input.GetAxis("Horizontal")はキーボードの上下左右を取得(ASDWキーや実際のコントローラでも可)
        //Horizontalは平行(上下)、Verticalは垂直(左右)
        vector.x = Input.GetAxis("Horizontal");
        vector.z = Input.GetAxis("Vertical");
        //高さ方向の判定はキーのｚとする
        if (Input.GetKey("z"))
        {
            vector.y = 1.0f;
        }
        else
        {
            vector.y = -1.0f;
        }
        //シーンの重力を入力ベクトルの方向に合わせて変化させる
        Physics.gravity = Gravity * vector.normalized * gravityScale;
    }
}
